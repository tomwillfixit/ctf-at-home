# What is Multipass ?

> "Multipass provides a command line interface to launch, manage and generally fiddle about with instances of Linux. The downloading of a minty-fresh image takes a matter of seconds, and within minutes a VM can be up and running."

*[The Register](https://www.theregister.com/2019/01/22/multipass/)*

# Install Multipass 

Installing Multipass is fast and simple. Instructions can be found [here](https://multipass.run).

# Launch the VM

The following command will start a Ubuntu 20.04 VM, install docker, docker-compose and start the CTF Challenges.

```
multipass launch --mem 4G --cpus 4 --disk 10G --cloud-init capture_the_flag.yml --name ctf 20.04
```

You'll see this error but it can be ignored :
```
launch failed: The following errors occurred:                                   
timed out waiting for initialization to complete
```
The cloud init has a default timeout of 5 minutes and pulling the images takes longer than 5 mins.

# Login to VM
```
multipass shell ctf

Follow the progress of cloud init :

sudo tail -f /var/log/cloud-init-output.log

Example Output : 

Pulling otp-backend (registry.gitlab.com/contribute2020ctf/challenges/oh-tipi:otp_master-v1-0-0)...
otp_master-v1-0-0: Pulling from contribute2020ctf/challenges/oh-tipi
Digest: sha256:a2ffee8166eb430c72c99f8ab6aee1cfb4d0d530c6b0e428d3b79927f9514b08
Status: Downloaded newer image for registry.gitlab.com/contribute2020ctf/challenges/oh-tipi:otp_master-v1-0-0
Pulling rst (registry.gitlab.com/contribute2020ctf/challenges/rst:level1-v1-0-0)...
level1-v1-0-0: Pulling from contribute2020ctf/challenges/rst
Digest: sha256:a7ac4fe14006323bc72307356870ca8851d60d13d23c473328ea63e25d3a6120
Status: Downloaded newer image for registry.gitlab.com/contribute2020ctf/challenges/rst:level1-v1-0-0
Pulling rstlevel2 (registry.gitlab.com/contribute2020ctf/challenges/rst:level2-v1-0-0)...
level2-v1-0-0: Pulling from contribute2020ctf/challenges/rst
Digest: sha256:bc52ed081228dcd8cef83ce994e6aebcd0d934376e095e0785d1aa9f0906d2fd
Status: Downloaded newer image for registry.gitlab.com/contribute2020ctf/challenges/rst:level2-v1-0-0
Pulling traefik (traefik:v2.2)...
v2.2: Pulling from library/traefik
Digest: sha256:f5af5a5ce17fc3e353b507e8acce65d7f28126408a8c92dc3cac246d023dc9e8
Status: Downloaded newer image for traefik:v2.2
Creating ctf-at-home_ssrf2_1    ... done
Creating ctf-at-home_otp-frontend_1 ... done
Creating ctf-at-home_rstlevel2_1  ... done
Creating ctf-at-home_rst_1     ... done
Creating ctf-at-home_graphql1_1   ... done
Creating ctf-at-home_aesgcm_1    ... done
Creating ctf-at-home_ssrf1_1    ... done
Creating ctf-at-home_ssrf3_1    ... done
Creating ctf-at-home_content_1   ... done
Creating ctf-at-home_tar2zip_1   ... done
Creating ctf-at-home_traefik_1   ... done
Creating ctf-at-home_otp-backend_1 ... done
Creating ctf-at-home_graphql2_1   ... done

```

# Start the Challenge

In order to access the CTF Challenges through your browser we need to redirect traffic on port 80 through to the appropriate challenge container.

*Step 1* : Find IP address of the ctf VM :
```
multipass info ctf |grep IP

export VMIP="<insert IP here>"
```

If you have [jq](https://stedolan.github.io/jq/) installed then you can run the following :
```
export VMIP=$(multipass info ctf --format json| jq '.info.ctf.ipv4[0]' | tr -d '\"')
```

*Step 2* : Use [socat](https://www.linux.com/news/socat-general-bidirectional-pipe-handler/) to make the challenges accessible through your browser on port 80.

Example : 

```
sudo socat TCP-LISTEN:80,fork,reuseaddr TCP-CONNECT:${VMIP}:80
```

Open your browser and navigate to : http://capture.local.thetanuki.io 

Good luck!

# Stopping and Removing the Multipass VM

When you're finished you can cleanup by running the following : 

```
multipass stop ctf
multipass delete --purge ctf
```

